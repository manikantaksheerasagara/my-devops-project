Database :

Amazon DocumentDB
Fully-managed MongoDB-compatible database service


DynamoDB
Managed NoSQL Database


ElastiCache
In-Memory Cache


Amazon Keyspaces
Serverless Cassandra-compatible database


Amazon MemoryDB for Redis
Fully managed, Redis-compatible, in-memory database service


Neptune
Fast, reliable graph database built for the cloud


Amazon QLDB
Fully managed ledger database


RDS
Managed Relational Database Service


Amazon Timestream
Amazon Timestream is a fast, scalable, and serverless time series database for IoT and operational applications.