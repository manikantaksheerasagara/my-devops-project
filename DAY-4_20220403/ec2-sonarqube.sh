#!/bin/bash 

# Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.xlarge" \
--count 1 \
--subnet-id "subnet-421fef29" \
--security-group-ids "sg-0d7f618c300003c9e" \
--key-name "staragile_aws" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=SCAT-Sonarqube},{Key=Type,Value=SCAT},{Key=CreatedBy,Value=Using-AWSCLI-UsingShellScript}]' \
--user-data file://install-sonar.txt --profile devops 