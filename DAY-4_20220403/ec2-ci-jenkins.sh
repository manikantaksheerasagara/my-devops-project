#!/bin/bash 

# Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-08d4ac5b634553e16" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-091669d0a1f197b40" \
--security-group-ids "sg-079f246deb12c837f" \
--key-name "20mar" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=CI-Jenkins},{Key=Type,Value=CI},{Key=CreatedBy,Value=Using-AWSCLI-UsingShellScript}]' \
--user-data file://install-jenkins.txt --profile mani1  